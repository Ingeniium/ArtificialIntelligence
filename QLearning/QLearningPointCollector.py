import torch
import argparse
import copy
import random
import math

LEFT = 0
RIGHT = 1
UP = 2
DOWN = 3
max_er = 1.
min_er = .1
"""
This is a game where there's a 2D board of spaces and each board space has a number of points.
The goal of the game is to maximize the number of points one can get given a certain number of moves
and a starting point. 
RULES:
You can only move one step at a time.
You can only move up, down, left, or right.
You can only move up to MAX_STEPS steps in each episode.
You must take all the points of a sqaure upon arriving on said square
"""
#Select the next action based on the current state
def PointCollectorGetAction(state_l, state_w, explore, Q_table):
	#If we're exploring, we pick a random direction to go, as long as it gets us to a space on the board
	actions = []
	if state_l > 0:
		actions.append(LEFT)
	if state_w > 0:
		actions.append(UP)
	if state_l < Q_table.shape[0] - 1:
		actions.append(RIGHT)
	if state_w < Q_table.shape[1] - 1:
		actions.append(DOWN)
	if explore: 
		return actions[random.randint(0, len(actions) - 1)]#Note, randint is inclusive for both a and b args
	#Otherwise, we'll do exploitation; we'll just get the direction that gets us the most immediate expected reward
	else:
		state_table = Q_table[state_l, state_w, :]
		#print(state_table)
		action = actions[0]
		for act in actions:
			if state_table[act] > state_table[action]:
				"""if (i in actions and m < state_table[i]) or ind not in actions:
				m = state_table[i]
				ind = i"""
				action = act
		#print(ind, state_table[ind])
		return action
		#return torch.argmax(state_table)
		
#Provides an update to the state given an action 
def PointCollectorGetNextState(state_l, state_w, action):
	if action == LEFT:
		return (state_l - 1, state_w)
	elif action == RIGHT:
		return (state_l + 1, state_w)
	elif action == UP:
		return (state_l, state_w - 1)
	else:
		return (state_l, state_w + 1)
		
def PointCollectorEpisode(Q_table, reward_table, max_steps, learning_rate, discount_rate, exploration_rate):
	board = copy.deepcopy(reward_table) #We want each episode to start with exact same board
	state_l = 0#Start out at upperleft corner of the board
	state_w = 0
	total_reward = 0
	for step in range(max_steps):
		explore = random.uniform(0, 1) < exploration_rate #Determine whether we explore or exploit
		action = PointCollectorGetAction(state_l, state_w, explore, Q_table)#Get our next action
		next_l, next_w = PointCollectorGetNextState(state_l, state_w, action)#Get our next state
		#print(next_l, next_w, explore)
		#assert explore == True or (explore == False and ((next_l < Q_table.shape[0] and next_w < Q_table.shape[1]) and (next_l > -1 and next_w > -1)))
		reward = board[next_l, next_w].float().item()#Reward will be the points we have to collect in our NEXT space
		board[next_l, next_w] = 0#Set to 0 so that we can only pick up a spot's points once
		#Update the current state's Q value
		#print(learning_rate * (reward + discount_rate * torch.argmax(Q_table[next_l, next_w, :])))
		#print(reward)
		Q_table[state_l, state_w, action] = Q_table[state_l, state_w, action] * (1 - learning_rate) + learning_rate * (reward + discount_rate * torch.max(Q_table[next_l, next_w, :]))#New Value
		#print(Q_table[state_l, state_w, action])
		#Update the state 
		state_l = next_l
		state_w = next_w
		total_reward += reward
	#print(Q_table)
	return total_reward, board
		
	
def PointCollector(length, width, learning_rate, discount_rate, max_steps, num_episodes, low=1, high=10):
	#Create reward table and initialize q table to all zeroes
	reward_table = torch.randint(size=(length, width), low=low, high=high)
	max_reward = 0
	Q_table = torch.zeros(length, width, 4)
	exploration_rate = max_er
	decay_rate = .9999
	for episode in range(num_episodes):
		episode_reward, end_board = PointCollectorEpisode(Q_table, reward_table, max_steps, learning_rate, discount_rate, exploration_rate)
		max_reward = max(max_reward, episode_reward)
		#Apply exploration rate decay; as episodes progress, Q learning should explore less and exploit more (become more confident)
		exploration_rate = (max_er - min_er) * math.exp(-.001 * episode) 
		print(exploration_rate)
		print("Episode " + str(episode) + "'s reward: " + str(episode_reward))
		#print(end_board)
	print("Initial point board table : ")
	print(reward_table)
	print("Most reward obtained: " + str(max_reward))
	
if __name__== "__main__":
	#NOTE, nargs = "?" means argument takes 0 or 1 arguments
	parser = argparse.ArgumentParser()
	parser.add_argument("--length", type=int, nargs="?", default=5)
	parser.add_argument("--width", type=int, nargs="?", default=5)
	parser.add_argument("--lr", type=float, nargs="?", default=.01)
	parser.add_argument("--max_steps", type=int, nargs="?", default=10)
	parser.add_argument("--episodes", type=int, nargs="?", default=5000)
	parser.add_argument("--low", type=int, nargs="?", default=1)
	parser.add_argument("--high", type=int, nargs="?", default=10)
	parser.add_argument("--dr", type=float, nargs="?", default=.99)
	args = parser.parse_args()
	
	PointCollector(args.length, args.width, args.lr, args.dr, args.max_steps, args.episodes, args.low, args.high)